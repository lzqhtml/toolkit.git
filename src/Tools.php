<?php

namespace elegant\toolkit;

class Tools
{
    /**
     * Actions: 二维数组排序
     * DateTime: 2024/9/25 17:58
     * @param array $array
     * @param string $keys
     * @param bool $asc
     * @return array
     * @author 李智强 <751468644@qq.com>
     */
    public static function arraySort(array $array, string $keys, bool $asc = true): array
    {
        $keyList = array_column($array, $keys);
        $rest = $asc ? SORT_ASC : SORT_DESC;
        array_multisort($keyList, $rest, $array);
        return $array;
    }
}
